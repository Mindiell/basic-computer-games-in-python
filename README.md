# Translation in Python of codes from book "BASIC Computer Games"

 * Author : David H. Ahl
 * Wikipedia : https://en.wikipedia.org/wiki/BASIC_Computer_Games
 * Licence : GPLV4+


# Translation rules

Even if some optimisation could have been done, the main idea is to copy the
code as it was originaly done.

Second goal is to have something like it was in the old days.


# List of games translated

2 on 96

 * AceyDucey    DONE
 * Amazing
 * Animal
 * Awari
 * Bagels
 * Banner
 * Basketball
 * Batnum
 * Battle
 * Blackjack
 * Bombardment
 * Bombs Away
 * Bounce
 * Bowling
 * Boxing
 * Bug
 * Bullfight
 * Bullseye
 * Bunny
 * Buzzword
 * Calendar
 * Change
 * Checkers
 * Chemist
 * Chief
 * Chomp
 * Civil War
 * Combat
 * Craps
 * Cube
 * Depth Charge
 * Diamond
 * Dice
 * Digits
 * Even Wins
 * Flip Flop
 * Football
 * Fur Trader
 * Golf
 * Gomoko
 * Guess
 * Gunner
 * Hammurabi    DONE
 * Hangman
 * Hello
 * Hexapawn
 * Hi-Lo
 * High I-Q
 * Hockey
 * Horserace
 * Hurkle
 * Kinema
 * King
 * Letter
 * Life
 * Life For Two
 * Literature Quiz
 * Love
 * Lunar LEM Rocket
 * Master Mind
 * Math Dice
 * Mugwump
 * Name
 * Nicomachus
 * Nim
 * Number
 * One Check
 * Orbit
 * Pizza
 * Poetry
 * Poker
 * Queen
 * Reverse
 * Rock, Scissors, Paper
 * Roulette
 * Russian Roulette
 * Salvo
 * Sine Wave
 * Slalom
 * Slots
 * Splat
 * Stars
 * Stock Market
 * Super Star Trek
 * Synonym
 * Target
 * 3-D Plot
 * 3-D Tic-Tac-Toe
 * Tic Tactoe
 * Tower
 * Train
 * Trap
 * 23 Matches
 * War
 * Weekday
 * Word