# encoding: utf-8
"""
ACEY DUCEY
From Basic Computer Games
By David H. Ahl
"""

import random

# Amount of dollars
START_AMOUNT = 100

# cards names
CARDS = {
    11: "JACK",
    12: "QUEEN",
    13: "KING",
    14: "ACE",
}


print("""           ACEY DUCEY CARD GAME
CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY



ACEY-DUCEY IS PLAYED IN THE FOLLOWING MANNER
THE DEALER (COMPUTER) DEALS TWO CARDS FACE UP
YOU HAVE AN OPTION TO BET OR NOT BET DEPENDING
ON WHETHER OR NOR YOU FEEL THE CARD WILL HAVE
A VALUE BETWEEN THE FIRST TWO.
IF YOU DO NOT WANT TO BET, INPUT A 0""")
dollars = START_AMOUNT
print(f"YOU NOW HAVE  {dollars}  DOLLARS")
print()
while True:
    print("HERE ARE YOUR NEXT TWO CARDS ")
    card_1 = random.randint(2, 13)
    card_2 = random.randint(3, 14)
    while card_1 >= card_2:
        card_1 = random.randint(2, 13)
    print("", CARDS.get(card_1, card_1))
    print("", CARDS.get(card_2, card_2))
    bet = -1
    while bet < 0:
        print()
        try:
            bet = int(input("WHAT IS YOUR BET? "))
        except:
            bet = -1
        if bet == 0:
            print("CHICKEN!!")
            print()
        elif bet > dollars:
            print("SORRY, MY FRIEND BUT YOU BET TOO MUCH")
            print(f"YOU HAVE ONLY {dollars} DOLLARS TO BET")
            bet = -1
    if bet > 0:
        card_3 = random.randint(2, 14)
        print(CARDS.get(card_3, card_3))
        if card_1 < card_3 < card_2:
            print("YOU WIN!!!")
            dollars += bet
        else:
            print("SORRY, YOU LOSE")
            dollars -= bet
            if dollars <= 0:
                print()
                print("SORRY, FRIEND BUT YOU BLEW YOUR WAD")
                answer = input("TRY AGAIN (YES OR NO)")
                if answer.upper() != "YES":
                    print("OK HOPE YOU HAD FUN")
                    break
                else:
                    dollars = START_AMOUNT
            else:
                print(f"YOU NOW HAVE  {dollars}  DOLLARS")
                print()
