# encoding: utf-8
"""
HAMURABI
From Basic Computer Games
By David H. Ahl
"""

import random
import sys


population = 95                 # P
immigrants = 5                  # I
starved = 0                     # D
year = 0                        # Z
harvested = 3000                # H
store = 2800                    # S
eat = harvested - store         # E
rate = 3                        # Y
acres = int(harvested / rate)   # A
plague = 1                      # Q
total_starved = 0               # D1
percent_starved = 0             # P1


def bad_hamurabi():
    print()
    print("HAMURABI:  I CANNOT DO WHAT YOU WISH.")
    print("GET YOURSELF ANOTHER STEWARD!!!!!")
    print()
    end()


def not_enough_grain():
    print("HAMURABI:  THINK AGAIN.  YOU HAVE ONLY")
    print(f"{store} BUSHELS OF GRAIN. NOW THEN,")


def not_enough_acres():
    print(f"HAMURABI:  THINK AGAIN.  YOU OWN ONLY {store} ACRES. NOW THEN,")


def impeachment():
    print("DUE TO THIS EXTREME MISMANAGEMENT YOU HAVE NOT ONLY")
    print("BEEN IMPEACHED AND THROWN OUT OF THE OFFICE BUT YOU HAVE")
    print("ALSO BEEN DECLARED NATIONAL FINK!!!!")
    end()


def unpleasant_ruler():
    print("YOUR HEAVY-HANDED PERFORMANCE SMACKS OF NERO AND IVAN IV.")
    print("THE PEOPLE (REMAINNG) FIND YOU AN UNPLEASANT RULER, AND,")
    print("FRANKLY, HATE YOUR GUTS!!")
    end()


def not_bad():
    print("YOUR PERFORMANCE COULD HAVE BEEN SOMEWHAT BETTER, BUT")
    print(f"REALLY WASN'T TOO BAD AT ALL. {random.randint(0, int(population*.8))} PEOPLE")
    print("DEARLY LIKE TO SEE YOU ASSASSINATED BUT WE ALL HAVE OUR")
    print("TRIVIAL PROBLEMS.")
    end()


def end():
    print("SO LONG FOR NOW.")
    print()
    sys.exit()


print("""           HAMURABI
CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY


""")
while True:
    year += 1
    print()
    print()
    print("HAMURABI:  I BEG TO REPORT TO YOU,")
    print(f"IN YEAR {year}, {starved} PEOPLE STARVED, {immigrants} CAME TO THE CITY,")
    population += immigrants
    if plague <= 0:
        population = int(population / 2)
        print("A HORRIBLE PLAGUE STRUCK!  HALF THE PEOPLE DIED.")
    print(f"POPULATION IS NOW {population}")
    print(f"THE CITY NOW OWNS {acres} ACRES.")
    print(f"YOU HARVESTED {rate} BUSHELS PER ACRE.")
    print(f"RATS ATE {eat} BUSHELS.")
    print(f"YOU NOW HAVE {store} BUSHELS IN STORE.")
    print()
    if year >= 11:
        print(f"IN YOUR 10-YEAR TER OF OFFICE, {percent_starved} PERCENT OF THE")
        print("POPULATION STARVED PER YEAR ON THE AVERAGE, I.E. A TOTAL OF")
        print(f"{total_starved} PEOPLE DIED!!")
        print("YOU STARTED WITH 10 ACRES PER PERSON AND ENDED WITH")
        print(f"{int(acres/population)} ACRES PER PERSON.")
        print()
        if percent_starved > 33:
            impeachment()
        elif acres / population < 7:
            impeachment()
        elif percent_starved > 10:
            unpleasant_ruler()
        elif acres / population < 9:
            unpleasant_ruler()
        elif percent_starved > 3:
            not_bad()
        elif acres / population < 10:
            not_bad()
        print("A FANTASTIC PERFORMANCE!!!  CHARLEMAGNE, DISRAELI, AND")
        print("JEFFERSON COMBINED COULD NOT HAVE DONE BETTER!")
        end()
    cost = random.randint(17, 26)
    print(f"LAND IS TRADING AT {cost} BUSHELS PER ACRE.")
    buy = -1
    while buy < 0:
        try:
            buy = int(input("HOW MANY ACRES DO YOU WISH TO BUY? "))
        except:
            bad_hamurabi()
        if buy < 0:
            bad_hamurabi()
        elif buy > 0:
            if buy * cost > store:
                not_enough_grain()
                buy = -1
            else:
                acres += buy
                store -= buy * cost
    if buy == 0:
        sell = -1
        while sell < 0:
            try:
                sell = int(input("HOW MANY ACRES DO YOU WISH TO SELL? "))
            except:
                bad_hamurabi()
            if sell < 0:
                bad_hamurabi()
            elif sell > 0:
                if sell > acres:
                    not_enough_acres()
                    sell = -1
                else:
                    acres -= sell
                    store += sell * cost

    feed = -1
    while feed < 0:
        try:
            feed = int(input("HOW MANY BUSHELS DO YOU WISH TO FEED YOUR PEOPLE? "))
        except:
            bad_hamurabi()
        if feed < 0:
            bad_hamurabi()
        elif feed > 0:
            if feed > store:
                not_enough_grain()
                feed = -1
            else:
                store -= feed
                C = 1

    plant = -1
    while plant < 0:
        try:
            plant = int(input("HOW MANY ACRES DO YOU WISH TO PLANT WITH SEED? "))
        except:
            bad_hamurabi()
        if plant < 0:
            bad_hamurabi()
        elif plant > 0:
            if plant > acres:
                not_enough_acres()
                plant = -1
            elif int(plant/2) > store:
                not_enough_grain()
                plant = -1
            elif plant > population * 10:
                print(f"BUT YOU HAVE ONLY {population} TO TEND THE FIELDS!  NOW THEN,")
                plant = -1
            else:
                store -= int(plant/2)

    rate = random.randint(1,5)
    harvested = rate * plant
    eat = 0
    result = random.randint(1,5)
    if result % 2 == 0:
        eat = int(store / result)
    store = store - eat + harvested
    immigrants = int(random.randint(1,5)*(20*acres+store)/population/100+1)
    plague = int(10*(random.randint(0,1)-.3))
    feeded = int(feed/20)
    if population >= feeded:
        starved = population - feeded
        if starved > population * .45:
            print()
            print(f"YOU STARVED {starved} PEOPLE IN ONE YEAR!!!")
            impeachment()

    percent_starved = ((year - 1) * percent_starved + starved * 100 / population) / year
    population = feeded
    total_starved += starved
